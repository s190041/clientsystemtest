#!/bin/bash
set -e
docker image prune -f
docker-compose up -d rabbitMq
sleep 10s
docker-compose up -d payment_service money_transfer_service token_service user_service

